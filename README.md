# Welcome to the WRM-Webpack example
In this example you can see how to integrate the [Atlassian Webresource Webpack Plugin](https://bitbucket.org/atlassianlabs/atlassian-webresource-webpack-plugin/src/master/) into your p2-plugin dev-loop.

## Follow the steps
The easiest way to see how it develops is to follow the tags from [step-1.1](https://bitbucket.org/serverecosystem/wrm-webpack-example/src/step-1.1/) to [step-8.1](https://bitbucket.org/serverecosystem/wrm-webpack-example/src/step-8.1/).
The bonus section is purely hypothetical and will not work locally.