import initWebpanel from './webpanel';

initWebpanel();

if (module.hot) {
    module.hot.accept('./webpanel', () => {
        require('./webpanel').default();
    });
}