import jquery from 'jquery';
import _ from 'lodash';
import image from '../../resources/martin-adams-763268-unsplash.jpg';

import styles from './style.less';

export const later = (elem) => {
    elem = _.head([elem]); // useless
    jquery(elem).html(`<img class="${styles.moon}" src=${image} />`);
}