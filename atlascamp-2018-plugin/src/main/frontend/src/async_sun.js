import jquery from 'jquery';
import _ from 'lodash';
import image from '../../resources/joel-henry-498313-unsplash.jpg';

import styles from './style.less';

export const later = (elem) => {
    elem = _.head([elem]); // useless
    jquery(elem).html(`<img class="${styles.sun}" src=${image} />`);
}