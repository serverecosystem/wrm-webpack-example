import attach from 'atlassian-webpanels-webpack-plugin/webpanel-factory';
import jQuery from 'jquery';

export default () => attach('My Atlascamp Webpanel!', (el) => {
    const getContainer = () => jQuery(el).find('.container').get(0);

    jQuery(el)
        .html(`
        <div class="container"></div>
        <button class="aui-button moon">Load Moon!</button>
        <button class="aui-button sun">Load Sun!</button>`)
        .on('click', '.moon', (event) => {
            import(/* webpackChunkName: "moon" */ './async_moon').then(async => async.later(getContainer()))
        })
        .on('click', '.sun', (event) => {
            import(/* webpackChunkName: "sun" */ './async_sun').then(async => async.later(getContainer()))
        });
});